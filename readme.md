# @usage-tracking-client/react-native

## installation

```sh
yarn add react-native-sqlite-storage
yarn add ssh://git@bitbucket.org:powerdesigninc/usage-tracking-client-react-native.git
# or
npm install react-native-sqlite-storage
npm install ssh://git@bitbucket.org:powerdesigninc/usage-tracking-client-react-native.git
```

## initialize

```tsx
import { UsageTracking } from '@usage-tracking-client/react-native';

class App extends React.Component {
  async componentDidMount() {
    await UsageTracking.initialize({
      app: app, // like 'CREWCONNECT'
      appVersion: version, // like '3.0.0'
      stage: stage, // one of ['prod', 'test', 'dev']
      authorizationToken: token,
      enabled: true | false,
      // others
    });
  }

  componentWillUnmount() {
    UsageTracking.stop();
  }
}
```

## record

```tsx
UsageTracking.record(event, {
  event: parameter,
});
```

> UsageTracking will record App Active and App Inactive
