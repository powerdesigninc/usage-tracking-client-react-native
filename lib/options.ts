import moment from 'moment';
import { Platform } from 'react-native';

import { UsageTrackingOptions } from './types';

export let options: UsageTrackingOptions;

export function initializeOptions(
  input: UsageTrackingOptions,
): UsageTrackingOptions {
  options = {
    stage: '',
    sessionDuration: moment.duration(1, 'day').asMilliseconds(),
    geoInfoDuration: moment.duration(1, 'day').asMilliseconds(),
    bulkSize: 100,
    deviceType: 'MOBILE',
    osVersion: `${Platform.OS}-${Platform.Version}`,
    ...input,
  };

  switch (input.stage.toLowerCase()) {
    case 'prod':
    case 'production':
      options.baseUrl = 'https://api.powerdesigninc.us/usage/v1';
      break;
    case 'test':
      options.baseUrl = 'https://servicestest.powerdesigninc.us/usage/v1';
      break;
    case 'uat':
      options.baseUrl = 'https://servicesuat.powerdesigninc.us/usage/v1';
      break;
    case 'dev':
      options.baseUrl = 'https://servicesdev.powerdesigninc.us/usage/v1';
      break;
    default:
      throw new Error(`Unknown stage: ${input.stage}`);
  }

  return options;
}
