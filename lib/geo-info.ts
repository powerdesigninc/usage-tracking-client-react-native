import axios from 'axios';

import AsyncStorage from '@react-native-community/async-storage';

import { log } from './log';
import { options } from './options';
import { GeoInfo, IpAPIResult } from './types';

const GeoInfoStorageKey = 'usage_tracking_geo_info';
const GeoInfoTimestampStorageKey = 'usage_tracking_geo_info_timestamp';

let geoInfo: GeoInfo;
let geoInfoTimestamp: number = 0;

export async function fetchGeoInfo(): Promise<GeoInfo> {
  // never fail
  await loadCache();

  if (geoInfoTimestamp + options.geoInfoDuration < Date.now()) {
    try {
      const res = await axios.get<IpAPIResult>('http://ip-api.com/json', {
        timeout: 2000,
      });

      const result = res.data;

      if (result.status !== 'success') {
        throw result;
      }

      geoInfo = {
        ipAddress: result.query,
        country: result.country,
        countryCode: result.countryCode,
        region: result.region,
        regionName: result.regionName,
        city: result.city,
        zip: result.zip,
        latitude: result.lat,
        longitude: result.lon,
        timezone: result.timezone,
        isp: result.isp,
      };

      AsyncStorage.multiSet([
        [GeoInfoStorageKey, JSON.stringify(geoInfo)],
        [GeoInfoTimestampStorageKey, Date.now().toString()],
      ]);
      log('geo info fetched', geoInfo);
    } catch (error) {
      log('fetch geo info failed', error);
    }
  }

  return geoInfo ?? {};
}

async function loadCache() {
  const result = await AsyncStorage.multiGet([
    GeoInfoStorageKey,
    GeoInfoTimestampStorageKey,
  ]);

  for (const [key, value] of result) {
    switch (key) {
      case GeoInfoStorageKey:
        if (value) {
          geoInfo = JSON.parse(value);
        }
        break;
      case GeoInfoTimestampStorageKey:
        if (value) {
          geoInfoTimestamp = parseInt(value, 10);
        }
        break;
    }
  }
}
