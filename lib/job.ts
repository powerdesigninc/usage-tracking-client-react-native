import { log } from './log';

type JobHandler = () => Promise<boolean>;

export class Job {
  private nextInterval: number;
  private timerID: number;

  constructor(private handler: JobHandler, private interval: number = 10000) {}

  get isCancelled() {
    return this.timerID == null;
  }

  start() {
    log('job started');

    if (this.timerID == null) {
      this.nextInterval = this.interval; // reset the interval time
      this.timerID = setTimeout(this.exec, this.nextInterval); // execute delayed
    }
  }

  stop() {
    log('job stopped');

    if (this.timerID != null) {
      clearTimeout(this.timerID);
      this.timerID = null;
    }
  }

  private exec = async () => {
    const result = await this.handler();

    if (result) {
      // if the result is true, execute the job again
      this.timerID = setTimeout(this.exec, this.nextInterval);
      this.nextInterval = this.nextInterval * 2; // double the interval time after failure
    } else {
      this.timerID = null;
    }
  };
}
