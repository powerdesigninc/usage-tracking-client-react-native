import { AppState, AppStateStatus } from 'react-native';

import { postEvents } from './api';
import { fetchGeoInfo } from './geo-info';
import { log } from './log';
import { initializeOptions, options } from './options';
import { Session } from './session';
import {
  deleteEvents,
  initializeDatabase,
  readEvents,
  writeEvent,
} from './storage';
import { Job } from './job';
import { EventRecord, UsageTrackingOptions } from './types';
import { convertParameters, getTimestamp } from './utils';

let isInitialized = false;
let previousState = null;

export const UsageTracking = {
  /**
   * call when componentDidMount
   */
  async initialize(initialOptions: UsageTrackingOptions): Promise<void> {
    try {
      initializeOptions(initialOptions);

      isInitialized = true;
      await Session.initialize();

      await initializeDatabase();

      if (options.autoFetchGeoInfo ?? true) {
        options.geoInfo = await fetchGeoInfo();
      }

      if (options.enabled) {
        start();
      }
    } catch (error) {
      log('initialization failed', error);
    }
  },

  get enabled() {
    return options?.enabled;
  },

  set enabled(value: boolean) {
    if (options.enabled === value) {
      return;
    }

    options.enabled = value;
    if (options.enabled) {
      start();
    } else {
      stop();
    }
  },

  /**
   * basic record event
   */
  record<T>(event: string, parameters?: T, triggerJob: boolean = true): void {
    if (!isInitialized) {
      console.warn('[UT] please call UsageTracking.initialize() first');
      return;
    }

    if (options.transform) {
      parameters = options.transform(event, parameters);
    }

    writeEvent({
      sessionID: Session.currentID,
      event: event,
      parameters: convertParameters(parameters),
      timestamp: getTimestamp(),
    });

    if (triggerJob) {
      job.start();
    }
  },
  /**
   * record navigation
   * */
  recordNavigate<T>(screen: string, parameters?: T): void {
    UsageTracking.record('NAVIGATE', {
      screen,
      ...parameters,
    });
  },
  /**
   * record navigation
   * */
  recordAction<T>(action: string, parameters?: T): void {
    UsageTracking.record('ACTION', {
      action,
      ...parameters,
    });
  },
  /**
   * record state
   * */
  recordState<T>(state: string, parameters?: T, triggerJob?: boolean): void {
    UsageTracking.record(
      'STATE',
      {
        state,
        ...parameters,
      },
      triggerJob,
    );
  },
  /**
   * record state
   * */
  recordLog<T extends object>(parameters: T): void {
    UsageTracking.record('LOG', parameters);
  },
};

const job = new Job(emitEvents);

async function start() {
  AppState.addEventListener('change', handleAppStateChange);

  if (!Session.valid()) {
    await Session.update();
  }

  // because there are many await, active state is not triggered for the first time
  handleAppStateChange('active');
}

function stop() {
  AppState.removeEventListener('change', handleAppStateChange);
}

function handleAppStateChange(appState: AppStateStatus) {
  if (previousState === appState) {
    return;
  }

  previousState = appState;
  switch (appState) {
    case 'active':
      UsageTracking.recordState('ACTIVE');
      break;
    case 'background':
      UsageTracking.recordState('INACTIVE', null, false);
      job.stop();
      break;
  }
}

async function emitEvents(): Promise<boolean> {
  log('emit event started');

  if (Session.currentID == null) {
    // do nothing, if no sessionID
    return;
  }

  try {
    // fetch events from storage
    let records: EventRecord[];

    do {
      if (job.isCancelled) {
        return false;
      }

      records = await readEvents();

      const data = records.map((r) => {
        const e = JSON.parse(r.json);

        // store event can without id, so put id afterward if empty
        return {
          instanceID: e.sessionID ?? Session.currentID,
          feature: e.event,
          timestamp: e.timestamp,
          parameters: e.parameters,
        };
      });

      if (data.length > 0) {
        // post events to API
        await postEvents(data);

        // delete events from storage
        await deleteEvents(records);
      }
    } while (records.length > 0);

    return false;
  } catch (error) {
    log('post events failed', error);
    return true;
  }
}
