import AsyncStorage from '@react-native-community/async-storage';

import { startSession } from './api';
import { log } from './log';
import { options } from './options';

const SessionIDStorageKey = 'usage_tracking_session_id';
const TimestampStorageKey = 'usage_tracking_timestamp';

let sessionID: number;
let sessionTimestamp: number = 0;

export const Session = {
  async initialize() {
    try {
      const result = await AsyncStorage.multiGet([
        SessionIDStorageKey,
        TimestampStorageKey,
      ]);

      for (const [key, value] of result) {
        switch (key) {
          case SessionIDStorageKey:
            if (value) {
              sessionID = parseInt(value, 10);
            }
            break;
          case TimestampStorageKey:
            if (value) {
              sessionTimestamp = parseInt(value, 10);
            }

            break;
        }
      }
    } catch (error) {
      log('session initialize failed', error);
    }
  },
  get currentID() {
    return sessionID;
  },
  set currentID(id: number) {
    sessionID = id;
    sessionTimestamp = Date.now();

    AsyncStorage.multiSet([
      [SessionIDStorageKey, sessionID ? sessionID.toString() : ''],
      [TimestampStorageKey, sessionTimestamp.toString()],
    ]);
  },
  valid() {
    return sessionID && sessionTimestamp + options.sessionDuration > Date.now();
  },
  update() {
    return startSession()
      .then(function (result) {
        log('session updated: sessionID=', result.instanceID);
        Session.currentID = result.instanceID;
      })
      .catch(function (error) {
        log('start session failed', error);
        Session.currentID = null;
      });
  },
};
