import { options } from './options';

export function log(...args) {
  if (options?.logEnabled) {
    console.log('[UT]', ...args);
  }
}
