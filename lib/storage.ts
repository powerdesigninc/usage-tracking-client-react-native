import SQLite, { SqliteDatabase } from 'react-native-sqlite-storage';

import { log } from './log';
import { options } from './options';
import { EventRecord, UsageTrackingEvent } from './types';

let db: SqliteDatabase;
function logError(error) {
  log('sqlite failed,', error);
}

export function initializeDatabase(): Promise<void> {
  return new Promise(function (resolve) {
    db = SQLite.openDatabase(
      {
        name: 'usage-tracking.db',
        location: 'default',
      },
      function () {
        log('database opened');

        db.executeSql(
          'CREATE TABLE IF NOT EXISTS events (id INTEGER PRIMARY KEY AUTOINCREMENT, json TEXT NOT NULL)',
          [],
          function () {
            log('table created');
            resolve();
          },
          function (error) {
            logError(error);
            resolve();
          },
        );
      },
      function (error) {
        logError(error);
        resolve();
      },
    );
  });
}

export function writeEvent(event: UsageTrackingEvent): void {
  db.executeSql(
    'INSERT INTO events (json) VALUES (?)',
    [JSON.stringify(event)],
    function () {
      log('event inserted', event);
    },
    logError,
  );
}

export async function readEvents(): Promise<EventRecord[]> {
  return new Promise(function (resolve) {
    db.executeSql(
      `SELECT * FROM events LIMIT ${options.bulkSize}`,
      [],
      function (result) {
        resolve(result.rows.raw());
      },
      function (error) {
        logError(error);
        resolve([]);
      },
    );
  });
}

export async function deleteEvents(events: EventRecord[]): Promise<void> {
  return new Promise(function (resolve) {
    const ids = events.map((e) => e.id).join(',');

    db.executeSql(
      `DELETE FROM events WHERE id in (${ids})`,
      [],
      function () {
        log('events deleted', events);
        resolve();
      },
      function (error) {
        logError(error);
        resolve();
      },
    );
  });
}
