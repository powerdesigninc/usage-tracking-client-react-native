import axios from 'axios';

import { options } from './options';
import {
  StartSessionData,
  UsageTrackingEventData,
  UsageTrackingStartResult,
} from './types';
import { getTimestamp } from './utils';

export async function startSession(): Promise<UsageTrackingStartResult> {
  const token = await options.authorizationToken();
  if (token == null) {
    throw new Error('No AuthorizationToken');
  }

  const data: StartSessionData = {
    timestamp: getTimestamp(),
    app: options.app,
    appVersion: options.appVersion,
    deviceType: options.deviceType,
    osVersion: options.osVersion,
    ...options.geoInfo,
  };

  const result = await axios.request({
    method: 'POST',
    url: '/start',
    baseURL: options.baseUrl,
    headers: {
      Authorization: token,
    },
    data,
  });

  return result.data;
}

export async function postEvents(
  data: UsageTrackingEventData[],
): Promise<void> {
  const token = await options.authorizationToken();
  if (token == null) {
    throw new Error('No AuthorizationToken');
  }

  return axios.request({
    method: 'POST',
    url: '/bulk',
    baseURL: options.baseUrl,
    headers: {
      Authorization: token,
    },
    data,
  });
}
