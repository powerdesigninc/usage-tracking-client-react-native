export type UsageTrackingOptions = {
  stage: string;
  app: string;
  enabled: boolean;

  /**
   * the async function return authorization token
   */
  authorizationToken: () => Promise<string>;

  /**
   * app version, default is current app version
   */
  appVersion: string;

  /**
   * default is MOBILE
   */
  deviceType?: string;

  /**
   * default use Platform to get value
   */
  osVersion?: string;

  /**
   * default is true
   */
  autoFetchGeoInfo?: boolean;

  /**
   * in milliseconds, default is a day.
   */
  sessionDuration?: number;

  /**
   * in milliseconds, default is a day.
   */
  geoInfoDuration?: number;

  /**
   * in milliseconds, default is 5 minutes.
   */
  emitInterval?: number;

  bulkSize?: number;

  geoInfo?: GeoInfo;

  baseUrl?: string;

  /**
   * print log to console.log
   */
  logEnabled?: boolean;

  /**
   * transform the parameters
   */
  transform: (event: string, parameters: any) => any;
};

export type StartSessionData = {
  app: string;
  appVersion: string;
  deviceType: string;
  osVersion: string;
  timestamp: string;
  country?: string;
  countryCode?: string;
  region?: string;
  regionName?: string;
  city?: string;
  zip?: string;
  latitude?: number;
  longitude?: number;
  timezone?: string;
  isp?: string;
  ipAddress?: string;
  parameters?: unknown;
};

export type UsageTrackingEventData = {
  instanceID: string;
  feature: string;
  timestamp: string;
  parameters?: unknown;
};

export type UsageTrackingStartResult = {
  instanceID: number;
};

export type UsageTrackingEvent = {
  sessionID: number;
  event: string;
  timestamp: string;
  parameters?: unknown;
};

export type IpAPIResult = {
  query: string;
  status: string;
  country: string;
  countryCode: string;
  region: string;
  regionName: string;
  city: string;
  zip: string;
  lat: number;
  lon: number;
  timezone: string;
  isp: string;
  org: string;
  as: string;
};

export type GeoInfo = {
  country?: string;
  countryCode?: string;
  region?: string;
  regionName?: string;
  city?: string;
  zip?: string;
  latitude?: number;
  longitude?: number;
  timezone?: string;
  isp?: string;
  ipAddress?: string;
};

export type EventRecord = {
  id: number;
  json: string;
};
