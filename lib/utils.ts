import moment from 'moment';

const TimestampFormat = 'MM.DD.YYYY HH:mm:ss';

/**
 * return current time in MM.DD.YYYY HH:mm:ss format
 */
export function getTimestamp() {
  return moment().format(TimestampFormat);
}

/**
 * Db require [{name, value}] format,
 * so convert { name1: value1, name2: value2 } to
 * [{name: name1, value: value1}, {name: name2, value: value2}]
 */
export function convertParameters(parameters: unknown) {
  if (!parameters) {
    return;
  }

  return Object.entries(parameters).map(([name, value]) => ({ name, value }));
}
