module 'react-native-sqlite-storage' {
  // only define type that used in project
  type ErrorCallback = (err) => void;
  type ResultSetRowList = {
    length: number;
    raw(): any[];
    item(index: number): any;
  };

  type ResultSet = {
    insertId: number;
    rowsAffected: number;
    rows: ResultSetRowList;
  };

  type OpenDatabaseOptions = {
    name: string;
    location: Location;
    createFromLocation?: number | string;
  };

  type SqliteDatabase = {
    transaction(scoped: (tx: SqliteTransaction) => void);
    executeSql: (
      statement: string,
      params: any[],
      success?: (result: ResultSet) => void,
      error?: ErrorCallback,
    ) => void;
  };

  type SqliteTransaction = {
    executeSql: (
      statement: string,
      params: any[],
      success?: (tx: SqliteTransaction, result: ResultSet) => void,
      error?: ErrorCallback,
    ) => void;
  };

  export function openDatabase(
    options: OpenDatabaseOptions,
    success: () => void,
    error: ErrorCallback,
  ): SqliteDatabase;
}
